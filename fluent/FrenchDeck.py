import collections

# code taken from "Fluent Python" Chapter 1

Card = collections.namedtuple('Card', ['rank', 'suit'])


class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    suits = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits for rank in self.ranks]

    def __len__(self):
        print("calling len(frenchDecks)")
        return len(self._cards)

    def __getitem__(self, position):
        print(f"calling frenchDecks[{position}]")
        return self._cards[position]


if __name__ == "__main__":
    decks = FrenchDeck()
    print(decks[2])  # call __getitems__
    from random import choice

    chosen = choice(decks)
    print(chosen)
    print(len(decks))

    for card in decks:
        print(card)

    for card in reversed(decks):
        print(card)

    print(Card('Q', 'hearts') in decks)

    suit_values = dict(spades=3, hearts=2, diamonds=1, clubs=0)


    def spades_high(c):
        rank_value = FrenchDeck.ranks.index(c.rank)
        return rank_value * len(suit_values) + suit_values[c.suit]


    for card in sorted(decks, key=spades_high):
        print(card)
