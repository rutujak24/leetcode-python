from typing import List
from bisect import bisect

class Solution:
    def jobScheduling(self, startTime: List[int], endTime: List[int], profit: List[int]) -> int:
        jobs = sorted(zip(startTime, endTime, profit), key=lambda j: j[1])  # sort by end time
        dp = [[0, 0]]
        for s, e, p in jobs:
            i = bisect(dp, [s + 1]) - 1  # i: index of the last non-intersecting job
            if dp[i][1] + p > dp[-1][1]:  # if smaller than the last one, there is no need to do it no need to record it
                dp.append([e, dp[i][1] + p])  # dp[i][0] end time , dp[i][1] profit
        return dp[-1][1]


if __name__ == "__main__":
    sol: Solution = Solution()
    ans = sol.jobScheduling([1, 2, 3, 3], [3, 4, 5, 6], [50, 10, 40, 70])
    print(ans)
