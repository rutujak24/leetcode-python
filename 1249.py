from collections import deque


class Solution:
    def minRemoveToMakeValid(self, s: str) -> str:
        stack = []
        for i, c in enumerate(s):
            if c == '(':
                stack.append(i)
            if c == ')':
                if len(stack) != 0 and s[stack[-1]] == '(':
                    stack.pop()
                else:
                    stack.append(i)

        builder = ''
        remove_set = set(stack)
        for i, c in enumerate(s):
            if i not in remove_set:
                builder += c

        return builder


if __name__ == "__main__":
    s = Solution()
    print(s.minRemoveToMakeValid("lee(t(c)o)de)"))
    print(s.minRemoveToMakeValid("))(()"))

